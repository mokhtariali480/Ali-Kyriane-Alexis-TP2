# Création d'un rôle IAM pour la fonction Lambda
resource "aws_iam_role" "s3_to_sqs_lambda_role" {
  name = "s3_to_sqs_lambda_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

# Politique IAM pour accéder au seau S3
resource "aws_iam_policy" "lambda_s3_access" {
  name = "lambda_s3_access"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "s3:*",
        Resource = "arn:aws:s3:::mon-seau-unique-12345/*"
      },
    ]
  })
}

# Attacher la politique IAM d'accès S3 au rôle IAM
resource "aws_iam_role_policy_attachment" "lambda_s3_access_attachment" {
  role       = aws_iam_role.s3_to_sqs_lambda_role.name
  policy_arn = aws_iam_policy.lambda_s3_access.arn
}

# Politique IAM pour envoyer des messages à la file d'attente SQS principale
resource "aws_iam_policy" "lambda_sqs_send" {
  name = "lambda_sqs_send"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = "sqs:SendMessage",
        Resource = aws_sqs_queue.main_queue.arn
      },
    ]
  })
}

# Attacher la politique IAM d'envoi SQS au rôle IAM
resource "aws_iam_role_policy_attachment" "lambda_sqs_send_attachment" {
  role       = aws_iam_role.s3_to_sqs_lambda_role.name
  policy_arn = aws_iam_policy.lambda_sqs_send.arn
}

# IAM Role for Lambda function
resource "aws_iam_role" "sqs_to_dynamo_lambda_role" {
  name = "sqs_to_dynamo_lambda_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

# IAM Policy to interact with SQS
resource "aws_iam_policy" "sqs_policy" {
  name = "sqs_policy"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes"
        ],
        Resource = aws_sqs_queue.main_queue.arn
      },
    ]
  })
}

# IAM Policy to interact with DynamoDB
resource "aws_iam_policy" "dynamodb_policy" {
  name = "dynamodb_policy"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "dynamodb:PutItem"
        ],
        Resource = aws_dynamodb_table.my_table.arn
      },
    ]
  })
}

# Attach SQS policy to Lambda role
resource "aws_iam_role_policy_attachment" "sqs_policy_attachment" {
  role       = aws_iam_role.sqs_to_dynamo_lambda_role.name
  policy_arn = aws_iam_policy.sqs_policy.arn
}

# Attach DynamoDB policy to Lambda role
resource "aws_iam_role_policy_attachment" "dynamodb_policy_attachment" {
  role       = aws_iam_role.sqs_to_dynamo_lambda_role.name
  policy_arn = aws_iam_policy.dynamodb_policy.arn
}

