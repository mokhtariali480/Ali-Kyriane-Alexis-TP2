resource "aws_s3_bucket" "my_bucket" {
  bucket = "mon-seau-unique-12345"
}

resource "aws_s3_object" "job_offers_folder" {
  bucket = aws_s3_bucket.my_bucket.bucket
  key    = "job_offers/"
}

# Ressource pour la notification S3
resource "aws_s3_bucket_notification" "lambda_notification" {
  bucket = aws_s3_bucket.my_bucket.id 

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn # Remplacez par la bonne référence à votre fonction Lambda
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = aws_s3_object.job_offers_folder.key
    filter_suffix       = ".csv"
  }
}



