resource "aws_sqs_queue" "dead_letter_queue" {
  name = "my-dead-letter-queue"
}

resource "aws_sqs_queue" "main_queue" {
  name = "my-main-queue"

  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.dead_letter_queue.arn
    maxReceiveCount     = 4
  })
}
resource "aws_lambda_event_source_mapping" "sqs_to_lambda" {
  event_source_arn = aws_sqs_queue.main_queue.arn  
  function_name    = aws_lambda_function.sqs_to_dynamo_lambda.function_name
}


