data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content  = "hello_world"
    filename = "dummy.txt"
  }
}

# Ressource pour la fonction Lambda
resource "aws_lambda_function" "s3_to_sqs_lambda" {
  function_name = "s3_to_sqs_lambda"
  handler       = "exports.handler" # Remplacez par le gestionnaire spécifique de votre code Lambda
  runtime       = "nodejs14.x"
  memory_size   = 512
  timeout       = 900

  filename         = "/home/ali/Desktop/TP Cloud/TP2/Ali-Kyriane-Alexis-TP2/terraform/empty_lambda_code.zip" # Remplacez par le chemin correct
  source_code_hash = filebase64sha256("/home/ali/Desktop/TP Cloud/TP2/Ali-Kyriane-Alexis-TP2/terraform/empty_lambda_code.zip") # Remplacez par le chemin correct

  role = aws_iam_role.s3_to_sqs_lambda_role.arn

  environment {
    variables = {
      QUEUE_URL = aws_sqs_queue.main_queue.url
    }
  }
}

# Permission pour permettre à S3 de déclencher la fonction Lambda
resource "aws_lambda_permission" "allow_s3_invocation" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_to_sqs_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.my_bucket.arn
}

# Lambda function triggered by SQS
resource "aws_lambda_function" "sqs_to_dynamo_lambda" {
  function_name = "sqs_to_dynamo_lambda"
  handler       = "index.handler" # Change to your actual handler location
  runtime       = "nodejs18.x"
  memory_size   = 512
  timeout       = 30

  # Replace with the actual path to your Lambda function's deployment package
  filename         = "/home/ali/Desktop/TP Cloud/TP2/Ali-Kyriane-Alexis-TP2/terraform/empty_lambda_code.zip"
  source_code_hash = filebase64sha256("/home/ali/Desktop/TP Cloud/TP2/Ali-Kyriane-Alexis-TP2/terraform/empty_lambda_code.zip")

  role = aws_iam_role.sqs_to_dynamo_lambda_role.arn

  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.my_table.name
    }
  }
}

# Assuming 'data_archive_file_empty_zip_code_lambda_output_path' is a variable in Terraform that
# holds the actual output path of the Lambda function's zip file

# IAM Permission for SQS to invoke the Lambda function
resource "aws_lambda_permission" "allow_sqs_to_invoke_lambda" {
  statement_id  = "AllowExecutionFromSQS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sqs_to_dynamo_lambda.function_name
  principal     = "sqs.amazonaws.com"
  source_arn    = aws_sqs_queue.main_queue.arn
}

# Ensure 'aws_sqs_queue.main_queue.arn' is replaced with the actual ARN of your SQS queue

